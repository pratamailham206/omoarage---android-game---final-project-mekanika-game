﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playSkill2 : MonoBehaviour
{
    //MOVING
    public float speed = 5;

    //touch
    public static float power = 12.5f;
    public static Vector3 touchPositions;


    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Enemy1Script>();
        GetComponent<MoveScript>();
    }

    // Update is called once per frame
    void Update()
    {
        FindClosestEnemy();
    }

    void FindClosestEnemy()
    {       
        transform.position = Vector2.MoveTowards(transform.position, touchPositions, speed * Time.deltaTime);
        if (transform.position.x == touchPositions.x && transform.position.y == touchPositions.y)
        {
            DestroyProjectile();
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("EnemyTroops"))
        {
            //decrease health

            other.gameObject.GetComponent<Enemy1Script>().health -= power;
            power += 0.2f;
            DestroyProjectile();

            /* FEEDBACK
            float lifeTime = 2;
            MoveScript.moveSpeed += 1;

            if (MoveScript.moveSpeed >= 10)
            {
                MoveScript.moveSpeed = 10;
            }

            lifeTime -= Time.deltaTime; 
            if(lifeTime <= 2 && lifeTime >= 0)
            {
                EnemyPlayer.energi -= power;
            }            

            DestroyProjectile();
            Debug.Log(power);


            Enemy1Script.startTimeBtwShots -= 1;
            if (Enemy1Script.startTimeBtwShots <= 1)
            {
                Enemy1Script.startTimeBtwShots = 1;
            }
            */
        }
    }

    void DestroyProjectile()
    {
        Destroy(gameObject);
    }
}
