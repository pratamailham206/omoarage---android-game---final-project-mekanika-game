# Omoarage - Android Game - Final Project Mekanika Game

## About

Omoarage is a Strategy game that a player defeat and destroy all enemy in the map to win the Game.

Created with Unity

## Genre

Action, Strategy, RPG

## Platform

Android

## Gameplay

the player can move the character by dragging and dropping at a specific position. the character can automaticly attack if if it is at a certain distance from the enemy.The character also can use 2 skill with different effect and different damage. if Player can use the skills as long as the mana hasn't run out. player can win the game by defeating all of enemy. player can lose if character' health is run out.

## Development Team
- Ilham Pratama


## Screenshot

![](images/Capture5.PNG)

![](images/Capture7.PNG)

![](images/Capture8.PNG)

