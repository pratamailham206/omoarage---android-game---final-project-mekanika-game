﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManaScript : MonoBehaviour
{
    Image Mana;
    float maxStorage = 100f;

    // Start is called before the first frame update
    void Start()
    {
        Mana = GetComponent<Image>();
        GetComponent<MoveScript>();
    }

    // Update is called once per frame
    void Update()
    {
        Mana.fillAmount = MoveScript.mana / maxStorage;
    }
}
