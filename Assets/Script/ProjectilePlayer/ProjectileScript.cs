﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileScript : MonoBehaviour
{
    //MOVING
    public float speed;

    //OBJECT
    private Transform player;
    private Vector2 target;
    
    //ATTACK
    public static float power = 1.5f;

    // Start is called before the first frame update
    void Start()
    {
        //GetComponent
        player = GameObject.FindGameObjectWithTag("Player").transform;
        target = new Vector2(player.position.x, player.position.y);

        //GET ANOTHER SCRIPT
        GetComponent<MoveScript>();
        GetComponent<energiMount>();
        GetComponent<ProjectileScriptPlayer>();
    }   

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector2.MoveTowards(transform.position, target, speed * Time.deltaTime);
        if(transform.position.x == target.x && transform.position.y == target.y)
        {
            DestroyProjectile();
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        //float timeLeft = 2;

        if(other.CompareTag("Player"))
        {
            MoveScript.health -= power;

            //NEGATIVE CONSTRUCTIVE
            ProjectileScriptPlayer.power += 0.5f;
            if(ProjectileScriptPlayer.power >= 8f)
            {
                ProjectileScriptPlayer.power = 8f;
            }

            //NEGATIVE DESTRUCTIVE
            MoveScript.moveSpeed -= 0.2f;
            if(MoveScript.moveSpeed == 2f)
            {
                MoveScript.moveSpeed = 2f;
            }

            DestroyProjectile();
            
        }
    }
    //ProjectileScriptPlayer.power += 1;

    void DestroyProjectile()
    {
        Destroy(gameObject);
    }
}
