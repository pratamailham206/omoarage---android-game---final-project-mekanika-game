﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FindEnemy : MonoBehaviour
{
    //ATTACK
    /*
    public float attackDistance;
    private Transform enemy;
    private float waitTime;
    public float startWaitTime;    
    */
    public GameObject projectile;


    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        FindClosestEnemy();
    }

    void FindClosestEnemy()
    {
        float distanceToClosestEnemy = Mathf.Infinity;
        Enemy1Script closestEnemy = null;
        Enemy1Script[] allEnemies = GameObject.FindObjectsOfType<Enemy1Script>();

        foreach (Enemy1Script currentEnemy in allEnemies)
        {
            float distanceToEnemy = (currentEnemy.transform.position - this.transform.position).sqrMagnitude;
            if (distanceToEnemy < distanceToClosestEnemy)
            {
                distanceToClosestEnemy = distanceToEnemy;
                closestEnemy = currentEnemy;
            }
        }

        Instantiate(projectile, transform.position, Quaternion.identity);

    }
}
