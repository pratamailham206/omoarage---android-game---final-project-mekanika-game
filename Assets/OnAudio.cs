﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnAudio : MonoBehaviour
{
    public GameObject music;
    // Start is called before the first frame update
    void Start()
    {
        music = GameObject.FindGameObjectWithTag("music");        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void audioOn()
    {
        if(AudioScript.onAudio == true)
        {
            music.GetComponent<AudioScript>().musicSource.Stop();
            //AudioScript.musicSource.Stop();
            AudioScript.onAudio = false;
        }

        else
        {
            music.GetComponent<AudioScript>().musicSource.Play();
            //AudioScript.musicSource.Play();
            AudioScript.onAudio = true;
        }
    }
}
