﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseScript : MonoBehaviour
{
    GameObject menu;
    bool menuActive = false;
    // Start is called before the first frame update
    void Start()
    {
        menu = GameObject.FindGameObjectWithTag("PauseMenu");
        menu.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if(menuActive == false)
        {
            menu.SetActive(false);
        }

        else if(menuActive == true)
        {
            menu.SetActive(true);
        }
    }

    public void clicked()
    {
        if (menuActive == false)
        {
            Debug.Log("show Menu");
            menuActive = true;
        }

        else
        {
            menuActive = false; 
        }
    }
}
