﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class energiMount : MonoBehaviour
{
    Image energiValue;
    float maxStorage = 100f;    

    void Start()
    {    
        energiValue = GetComponent<Image>();
        GetComponent<MoveScript>();
    }

    // Update is called once per frame
    void Update()
    {        
        energiValue.fillAmount = MoveScript.health / maxStorage;
    }
}
